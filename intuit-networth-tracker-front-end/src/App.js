import React from 'react';
import './App.css';
import Tracker from './components/Tracker.js';

export default class App extends React.Component{
  render() {
    return (
      <Tracker/>
    );
  }
}
