import * as service from '../components/api/service'
import Axios from 'axios';

beforeEach(() => {
    fetch.resetMocks();
})

it("fetches the current networth", async () => {
    fetch.mockResponseOnce(JSON.stringify(10000));
  
    const networth = await service.fetchNetworth("CAD");
  
    expect(networth).toEqual(10000);
    expect(fetch).toHaveBeenCalledTimes(1);
  });

it("fetches the current liability total", async () => {
    fetch.mockResponseOnce(JSON.stringify(10000))
    const liability_amount = await service.fetchLiabilityAmount("CAD")
    expect(liability_amount).toEqual("10000.00");
    expect(fetch).toHaveBeenCalledTimes(1);
})


it("fetches the current asset total", async () => {
    fetch.mockResponseOnce(JSON.stringify(10000))
    const asset_amount = await service.fetchAssetAmount("CAD")
    expect(asset_amount).toEqual("10000.00");
    expect(fetch).toHaveBeenCalledTimes(1);
})

it("fetches all items", async () => {
    fetch.mockResponseOnce(JSON.stringify([{itemId: 1, amount: 100, type: "asset_short", name:'mock_item' }]))
    const items = await service.fetchAllItems("CAD")
    expect(items).toEqual([{itemId: 1, amount: "100.00", type: "asset_short", name:'mock_item' }]);
    expect(fetch).toHaveBeenCalledTimes(1);
    
})