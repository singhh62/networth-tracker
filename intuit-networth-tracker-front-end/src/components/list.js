import React from 'react'
import { addItem} from './api/service'
import Item from './item.js'
import {isValidName, isValidAmount } from './api/api'

export default class List extends React.Component {
    state = {
        nameToAdd: "",
        amountToAdd: "",
        currency: this.props.currency,
    }

    componentDidUpdate = (prevProps) => { 
        if (this.props.currency !== prevProps.currency) {
            this.setState({
                currency: this.props.currency
            })
        }
    }
    
    getType = () => {
        if (this.props.type === 'asset_long') {
            return "Long Term Asset"
        }
        else if (this.props.type === 'asset_short') {
            return "Short Term Asset"
        }
        else if (this.props.type === 'liability_long') {
            return "Long Term Debt"
        }
        else{
            return "Short Term Liability"
        }
    }
    
    theChangeFunc = (event) => {
        if(event.target.name === 'nameToAdd'){
            this.setState({
                nameToAdd : event.target.value
            })
        }
        else{
            this.setState({
                amountToAdd: event.target.value
            })
        }
    }

    addNewItem = async (event) => {
        if (isValidAmount(this.state.amountToAdd) && isValidName(this.state.nameToAdd)){
            await addItem({
                amount: this.state.amountToAdd,
                name: this.state.nameToAdd,
                type: this.props.type,
            }, this.state.currency)
        }
        else {
            alert("Invalid Input  Entered: Name("+this.state.nameToAdd + ") Amount(" + this.state.amountToAdd +")")
            event.preventDefault()
        }
    }

    render() {
        return (
            <div className='item-list'>
                {
                    this.props.items
                    .filter(item => item.type===this.props.type)
                    .map(item => <Item key={item.itemId} currency={this.props.currency} itemId={item.itemId} name={item.name} amount={item.amount} type={item.type}/>)
                }
                <form>
                    <input type="text" name='nameToAdd' placeholder={'Enter ' + this.getType()} value={this.state.nameToAdd} onChange={this.theChangeFunc}/>
                    ${this.state.currency} <input type="number" min='0' name='amountToAdd' placeholder="Enter Amount" value={this.state.amountToAdd} onChange={this.theChangeFunc}/>
                    <button className="button1" type="submit" onClick={this.addNewItem}>ADD</button>
                </form>
                <hr/>
            </div>
        )
    }
}
