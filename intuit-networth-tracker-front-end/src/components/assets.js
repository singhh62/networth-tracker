import React from 'react'
import List from './list.js'
import '../stylesheet.css'
import { fetchAssetAmount } from './api/service.js'

export default class Assets extends React.Component {
    state = {
        total: 0,
        classNameForTotal: ''
    }

    componentDidMount = async () => {
        this.collectNewTotal()
    }

    componentDidUpdate = (prevProps) => { 
        if (this.props.currency !== prevProps.currency) {
            this.collectNewTotal()
        }
    }

    collectNewTotal = async () => {
        let total = await fetchAssetAmount(this.props.currency)
        this.setState({total}, this.changeColor)
    }

    render() {
        return (
            <div>
                <h2>Assets</h2>
                <hr/>
                <h4>Cash and Investments</h4>
                <hr/>
                <List items={this.props.data} currency={this.props.currency} type="asset_short"/>
                <h4>Long Term Assets</h4>
                <hr/>
                <List items={this.props.data} currency={this.props.currency} type="asset_long" />
                <h2 className="green-amount">Total Assets: ${(this.state.total).toLocaleString()} {this.props.currency}</h2>
                <hr/>
            </div>
        )
    }
}
