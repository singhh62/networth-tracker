import {fetchAssetAmount} from './service.js'

export const format_amount = (num) => {
    
    return (num.toFixed(2)).toLocaleString()
}
 
export const processedResult = (item) => {
    return {
        itemId: item.itemId,
        name: item.name,
        amount: format_amount(+item.amount),
        type: item.type
    }
}

export const collectAssets = (data) => { 
    return data.filter(data => data.type==="asset_long" || data.type === "asset_short")
}

export const collectLiabilities = (data) => { 
    return data.filter(data => data.type==="liability_long" || data.type === "liability_short")
}

export const collectAssetAmount = (cur) => {
    let value = fetchAssetAmount(cur)
    return value;
}

export const isValidAmount = (amount) => {
    return (amount > 0 && !isNaN(amount) && amount.toString().length < 19 && amount.toString().contains("e"))

}
export const isValidName = (name) => {
    return (name !== "" && name)
}