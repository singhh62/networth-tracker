import {processedResult} from './api.js'
var axios = require('axios');


export const fetchAllItems = async (currency) => {
    let response = await fetch('/api/items/' + currency).then(response=> response.json())
    return response.map(item => processedResult(item))        
}
export const fetchAssetAmount = async (currency) => { 
    return(
        await fetch('/api/items/assets/amount/' + currency)
            .then(response => response.json())
            .then(response => (response).toFixed(2))
    )
}
export const fetchLiabilityAmount = async (currency) => {
    return(
        await fetch('/api/items/liabilities/amount/' + currency)
        .then(response => response.json())
        .then(response => (response).toFixed(2))
    )
}

export const fetchNetworth = async (currency) => {
    console.log('getting networth')
    return await fetch('/api/networth/' + currency).then(response => response.json())
}

export const updateItem = async (data, currency) => {
    alert("sending as " + currency)
    await axios.put('/api/items/' + currency, data).then(response => response.json())
}

export const addItem = async (data, currency) => {
    await axios.post('/api/items/' + currency, data).then(response => response.json())
}

export const deleteItem = async (id) => {
    await axios.delete('/api/items/' + id)
}