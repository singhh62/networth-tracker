import React from 'react'
import List from './list.js'
import {fetchLiabilityAmount} from './api/service'

export default class Liabilites extends React.Component {
    state = {
        total: 0,
        classNameForTotal:''
    }

    componentDidMount = async () => {
        this.collectNewTotal()
    }
    
    componentDidUpdate = (prevProps) => { 
        if (this.props.currency !== prevProps.currency) {
            this.collectNewTotal()
        }
    }

    collectNewTotal = async () => {
        let total = await fetchLiabilityAmount(this.props.currency)
        this.setState({total}, this.changeColor)
    }

    render() {
        return (
            <div>
                <h2>Liabilities</h2>
                <hr/>
                <h4>Short Term Liabilites</h4>
                <hr/>
                <List items={this.props.data} currency={this.props.currency} type="liability_short"/>
                <h4>Long Term Debt</h4>
                <hr/>
                <List items={this.props.data} currency={this.props.currency} type="liability_long"/>
                <h2 className="red-amount">Total Liabilites: ${(this.state.total).toLocaleString()} {this.props.currency}</h2>
                <hr/>
            </div>
        )
    }
}
