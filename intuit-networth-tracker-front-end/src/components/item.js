import React from 'react'
import { updateItem, deleteItem} from './api/service'
import {isValidAmount} from './api/api'

export default class Item extends React.Component {
    state = {
        amount:  this.props.amount,
        itemId: this.props.itemId,
        currency: this.props.currency
    }

    componentDidUpdate = (prevProps) => { 
        if (this.props.amount !== prevProps.amount) {
            this.setState({
                amount: this.props.amount,
                currency: this.props.currency
            })
        }
    }

    theChangeFunc = (event) =>{
        this.setState({amount: event.target.value})
    }

    updateRecord= async () => {
        await updateItem({
            itemId: this.state.itemId,
            amount: this.state.amount,
            name: this.props.name,
            type: this.props.type
        }, this.state.currency)
    }

    deleteRecord = async () => {
        await deleteItem(this.state.itemId)
    }

    onClickButton = (event) => {
        if (event.target.name === 'update'){
            if (isValidAmount(this.state.amount)){
                this.updateRecord()
            }
            else{
                alert("Invalid Amount Entered. Please Try Again.")
                event.preventDefault()
            }
        }
        else{
            this.deleteRecord()
        }
    }

    render(){
        return (
            <div className='row-container'>
                <div className='item-name'>{this.props.name}</div> 
                <form className='row-form'>
                    ${this.state.currency}<input className='amount-edit-box' type="number" value={this.state.amount} onChange={this.theChangeFunc}/>
                    <button className='button1' type='submit' name='update' onClick={this.onClickButton}> OK </button>
                    <button className='button3' type='submit' name='delete' onClick={this.onClickButton}> DELETE </button>
                </form>
            </div>
        )
    }
}