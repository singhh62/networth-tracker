import React from 'react';
import Assets from './assets.js';
import Liabilities from './liabils.js';
import Networth from './networth.js'
import '../stylesheet.css'
import {fetchAllItems, fetchNetworth} from './api/service.js'
import {collectAssets, collectLiabilities} from './api/api.js'


export default class Tracker extends React.Component {

    state = {
        assets: [],
        liabilities: [], 
        currency: "CAD",
        networth: 0
    }

    componentDidMount = () => { 
        this.setItemsStates()
    }

    currencyChange = (event) => {
        this.setState({currency: event.target.value}, ( () => this.setItemsStates()))
    }

    setItemsStates = async () => {
        let items = await fetchAllItems(this.state.currency);
        console.log(items)
        let assets = collectAssets(items)
        let liabilities = collectLiabilities(items)
        this.setState({assets, liabilities}, () => this.calcNetworth(this.state.currency))
    }

    calcNetworth = async (currency) => {
        let networth = await fetchNetworth(currency)
        this.setState({networth})
    }

    render() { 
        return (
            <div className='main-container'>
                <h1>Tracking Your Networth</h1>
                <div className="currency-view">
                    Select Currency: <select defaultValue={this.state.currency} onChange={this.currencyChange}>
                        <option value="CAD">CAD</option>
                        <option value="USD">USD</option>
                    </select>
                <hr/>
                </div>
                <Networth currency={this.state.currency} amount={this.state.networth}/>
                <Assets currency={this.state.currency} data={this.state.assets}/>
                <Liabilities currency={this.state.currency} data={this.state.liabilities}/>
            </div>
        )
    }
}