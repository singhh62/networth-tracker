import React from 'react'

const Networth = (props) => {
    let classNameForTotal = ''
    if (props.amount < 0) {
        classNameForTotal = 'red-amount'
    }
    else{
        classNameForTotal = 'green-amount'
    }

    return (
        <div>
            <h2 className={classNameForTotal}>Networth: ${(props.amount.toFixed(2)).toLocaleString()} {props.currency}</h2>
            <hr/>
        </div>
    )
}
export default Networth