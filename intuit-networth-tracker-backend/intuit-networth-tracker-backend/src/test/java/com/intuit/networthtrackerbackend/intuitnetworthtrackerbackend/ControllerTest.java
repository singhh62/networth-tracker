package com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend;

import com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.entities.Item;
import com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.rest.TrackerController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static java.util.Collections.singletonList;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(TrackerController.class)
public class ControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TrackerController controller;

    @Test
    public void getItemsTest() throws Exception {
        Item item= new Item();
        item.setItemId((long) 1);
        item.setName("cash");
        item.setAmount(1);
        item.setType("asset_short");

        List<Item> allItems = singletonList(item);

        given(controller.getItems("CAD")).willReturn(allItems);

        mvc.perform(get("http://localhost:8080/items")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].itemId", is(1)));
    }
    @Test
    public void getTotalAssetAmountTest() throws Exception {
        Item item= new Item();
        item.setItemId((long) 1);
        item.setName("cash");
        item.setAmount(1);
        item.setType("asset_short");

        Item item2= new Item();
        item2.setItemId((long) 2);
        item2.setName("cash2");
        item2.setAmount(100);
        item2.setType("asset_short");


        List<Item> allItems = Arrays.asList(item, item2);
        double amountInCad = allItems.stream().mapToDouble(Item::getAmount).sum();
        when(controller.getTotalAssetAmount("CAD")).thenReturn(amountInCad);

        mvc.perform(get("http://localhost:8080/items/assets/amount/CAD")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(101.0)));
    }
    @Test
    public void getTotalLiabilityAmountTest() throws Exception {
        Item item= new Item();
        item.setItemId((long) 1);
        item.setName("credit card 2");
        item.setAmount(1);
        item.setType("liability_short");

        Item item2= new Item();
        item2.setItemId((long) 2);
        item2.setName("credit card 1");
        item2.setAmount(100);
        item2.setType("liability_short");


        List<Item> allItems = Arrays.asList(item, item2);
        double amountInCad = allItems.stream().mapToDouble(Item::getAmount).sum();
        when(controller.getLiabilityAmount("CAD")).thenReturn(amountInCad);

        mvc.perform(get("http://localhost:8080/items/liabilities/amount/CAD")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(101.0)));
    }


}
