package com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.services;

import com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.entities.Item;
import com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    private ItemRepository repository;

    @Autowired
    public ItemServiceImpl(ItemRepository repo){
        this.repository = repo;
    }

    @Override
    public Iterable<Item> getAllItems(String currency) {
        Iterable<Item> items = this.repository.findAll();
        if (currency.equals("USD")){
            items.forEach(item -> item.setAmount(item.getAmount()/1.35896));
        }
        return items;
    }

    @Override
    public double getTotalAssetAmount(String currency) {

        List<Item> assets = this.repository.findAllByTypeContains("asset");

        double amountInCad = assets.stream().mapToDouble(Item::getAmount).sum();
        if (currency.equals("USD")) {
            return amountInCad/1.35896;
        }
        return amountInCad;
    }

    @Override
    public double getTotalLiabilityAmount(String currency) {
        List<Item> liabilities = this.repository.findAllByTypeContains("liability");
        double amountInCad = liabilities.stream().mapToDouble(Item::getAmount).sum();
        if (currency.equals("USD")) {
            return amountInCad/1.35896;
        }
        return amountInCad;
    }

    @Override
    public double getNetworth(String currency) {
        List<Item> assets = this.repository.findAllByTypeContains("asset");
        List<Item> liabilities = this.repository.findAllByTypeContains("liability");
        double asset_sum = assets.stream().mapToDouble(Item::getAmount).sum();
        double liability_sum = liabilities.stream().mapToDouble(Item::getAmount).sum();

        if (currency.equals("USD")) {
            return (asset_sum - liability_sum) / 1.35896;
        } else {
            return asset_sum - liability_sum;
        }
    }

    @Override
    public ResponseEntity RemoveItemById(Long id) {
        this.repository.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Item> updateItemAmount(Item item, String currency) {

        if (!currency.equals("CAD")) {
            item.setAmount(item.getAmount() * 1.35896);
        }
        item.setAmount(item.getAmount());
        this.repository.save(item);
        return new ResponseEntity<>(item, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Item> addItem(Item item, String currency) {
        if (currency.equals("USD")) {
            item.setAmount(item.getAmount() * 1.35896);
        }
        item.setAmount(item.getAmount());
        this.repository.save(item);
        return new ResponseEntity<>(item, HttpStatus.CREATED);
    }
}
