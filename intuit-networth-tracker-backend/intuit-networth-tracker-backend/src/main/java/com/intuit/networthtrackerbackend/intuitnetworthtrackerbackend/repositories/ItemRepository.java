package com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.repositories;

import com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.entities.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {

    List<Item> findAllByTypeContains(String type);
}
