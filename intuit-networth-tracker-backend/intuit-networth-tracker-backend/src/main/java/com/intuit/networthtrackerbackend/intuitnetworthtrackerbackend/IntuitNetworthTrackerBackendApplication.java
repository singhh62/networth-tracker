package com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntuitNetworthTrackerBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntuitNetworthTrackerBackendApplication.class, args);
	}

}
