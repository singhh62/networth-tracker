package com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.rest;

import com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.entities.Item;
import com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class TrackerController {

    private ItemService service;

    @Autowired
    public TrackerController(ItemService service) {
        this.service = service;
    }

    @GetMapping("/api/items/{currency}")
    public Iterable<Item> getItems(@PathVariable String currency) {
        return this.service.getAllItems(currency);
    }

    @GetMapping("/api/items/assets/amount/{currency}")
    public double getTotalAssetAmount(@PathVariable String currency) {
        return service.getTotalAssetAmount(currency);
    }

    @GetMapping("/api/items/liabilities/amount/{currency}")
    public double getLiabilityAmount(@PathVariable String currency) {return service.getTotalLiabilityAmount(currency);}

    @GetMapping("/api/networth/{currency}")
    public double getNetworth(@PathVariable String currency) {return service.getNetworth(currency);}

    @DeleteMapping("/api/items/{id}")
    public ResponseEntity removeAccountingItemById(@PathVariable Long id) { return service.RemoveItemById(id);}

    @PutMapping("/api/items/{currency}")
    public ResponseEntity<Item> updateAmount(@PathVariable String currency, @RequestBody Item item) {
        return service.updateItemAmount(item, currency);}

    @PostMapping("/api/items/{currency}")
    public ResponseEntity<Item> addAccountItem(@PathVariable String currency, @RequestBody Item item) { return service.addItem(item, currency);}
}
