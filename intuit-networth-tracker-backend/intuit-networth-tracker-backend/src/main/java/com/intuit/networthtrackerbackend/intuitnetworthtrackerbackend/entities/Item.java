package com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.entities;
import javax.persistence.*;

@Entity
@Table(name="items")
public class Item {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="itemid", nullable = false, unique = true)
    private Long itemid;
    @Column(name="name")
    private String name;
    @Column(name="type")
    private String type;
    @Column(name="amount")
    private double amount;

    public Long getItemId() {
        return itemid;
    }

    public void setItemId(Long itemId) {
        this.itemid = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
