package com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.services;

import com.intuit.networthtrackerbackend.intuitnetworthtrackerbackend.entities.Item;
import org.springframework.http.ResponseEntity;

public interface ItemService {
    public Iterable<Item> getAllItems(String currency);

    public double getTotalAssetAmount(String currency);

    public double getTotalLiabilityAmount(String currency);

    public double getNetworth(String currency);

    public ResponseEntity RemoveItemById(Long id);

    public ResponseEntity<Item> updateItemAmount(Item item, String currency);

    public ResponseEntity<Item> addItem(Item item, String currency);

}
